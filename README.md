# OpenML dataset: GCM

https://www.openml.org/d/1106

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Multiclass cancer diagnosis using 16063 tumor gene expression signatures.
PNAS, VOL 98, no 26, pp. 15149-15154, December 18, 2001.

S. Ramaswamy, P. Tamayo, R. Rifkin, S. Mukherjee, C.-H. Yeang, M. Angelo, C. Ladd, M. Reich, E. Latulippe, J.P. Mesirov, T. Poggio, W. Gerald, M. Loda, E.S. Lander and T.R. Golub.

Original split: 144 first instances for training, remainder for testing

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1106) of an [OpenML dataset](https://www.openml.org/d/1106). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1106/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1106/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1106/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

